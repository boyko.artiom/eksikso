package net.esperonova.plugins.eksikso;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class EksiksoPlugin extends JavaPlugin implements Listener {
	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
	}

	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		String originalMessage = event.getMessage();
		String newMessage = originalMessage.replace("cx", "ĉ").replace("gx", "ĝ").replace("hx", "ĥ").replace("jx", "ĵ").replace("sx", "ŝ").replace("ux", "ŭ");
		event.setMessage(newMessage);
	}
}
